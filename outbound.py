#!/usr/bin/env python3

import requests
from requests.exceptions import HTTPError
import json

thorchain_url = 'https://thornode.thorchain.info/thorchain/'


response = requests.get('https://thornode.thorchain.info/thorchain/queue/outbound')
response.raise_for_status()
out = json.loads(response.text)
    
i = 0
total_i = 0
yggdrasil = 0
swap_pending = 0
swap_refund = 0
withdraw = 0
other_chain = 0
for outbound in out:
    total_i += 1
    if outbound['chain'] == 'ETH' : # and transaction['coin']['asset'] == 'ETH.ETH' :

        if outbound['memo'].startswith('YGGDRASIL'):
            yggdrasil += 1
            
            print('%3d YGGDRASIL - TO: %s' % (total_i, outbound['to_address']))

            continue

        inbound = json.loads(requests.get(thorchain_url + 'tx/' + outbound['in_hash']).text)

        if inbound['observed_tx']['tx']['memo'].startswith('SWAP') or inbound['observed_tx']['tx']['memo'].startswith('=') or inbound['observed_tx']['tx']['memo'].startswith('s'):
           
            if outbound['memo'].startswith('REFUND'):
                status='REFUND'
                swap_refund += 1
            if outbound['memo'].startswith('OUT'):
                status='PENDING'
                swap_pending += 1

            amount = int(outbound['coin']['amount'])/1e8
        
            print('%3d SWAP (%s) - TO: %s' % (total_i, status, outbound['to_address'])) 
            print('\t Input Coin  : %10s \tInput Amount  : %f ' % (inbound['observed_tx']['tx']['coins'][0]['asset'].split('-')[0], int(inbound['observed_tx']['tx']['coins'][0]['amount'])/1e8))
            print('\t Output Coin : %10s \tOutput Amount : %f ' % (outbound['coin']['asset'].split('-')[0], int(outbound['coin']['amount'])/1e8)) 

        if inbound['observed_tx']['tx']['memo'].startswith('WITHDRAW') or inbound['observed_tx']['tx']['memo'].startswith('-'):
            withdraw += 1

            print('%3d WITHDRAW - TO: %s' % (total_i, outbound['to_address']))

    else:
        other_chain += 1

print('Transations:')
print('\tYGGDRASIL      : %s' % yggdrasil)
print('\tSWAP (pending) : %s' % swap_pending)
print('\tSWAP (refund)  : %s' % swap_refund)
print('\tWITHDRAW       : %s' % withdraw)
print('\tOther Chain    : %s' % other_chain)

print('Total transactions: %d' % len(out))



